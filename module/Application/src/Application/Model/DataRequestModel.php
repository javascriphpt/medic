<?php

namespace Application\Model;

use Zend\Db\Sql\Expression;

class DataRequestModel extends Table
{

    private $_name = 'hr_datarequest';

    public function getDataRequest( $id ) {
        $where = array(
            'id'     => $id,
            'status' => 'A'
        );

        $select = $this->select()
                        ->from( $this->_name)
                        ->where( $where );

        return $this->fetchRowToArray($select);
    }

    public function selectFromDataSet( $config ) {

        $dataSet = $config['dataSet'];
        $where   = $config['where'];
        $offset  = !empty( $config['offset'] )? $config['offset'] : 0;
        $limit   = !empty( $config['limit'] )? $config['limit'] : 0;
        $order   = !empty( $config['order'] )? $config['order'] : array();

        $select = $this->select()
                        ->from( $dataSet);

        if( !is_null( $where ) ) {
            $select->where( $where );
        }

        if( !empty( $offset ) && !empty( $limit ) ) {
            $select->limit( $limit )
                    ->offset( $offset );
        }

        return $this->fetchAllToArray($select);
    }

    public function executeSP( $config ) {
        $spDataSet = $config['spDataSet'];
        $spParams  = $config['spParams'];

        $statement = $this->_dbAdapter->createStatement('CALL ' . $spDataSet . '( ' . $spParams . ' )');

        // var_dump($statement);
        $result = $this->fetchAllToArray( $statement );
        
        return !empty( $result )? $result : array();
    }

    /*public function selectFromSPDataSet( $config ) {
        $spDataSet = $config['spDataSet'];
        $spParams  = $config['spParams'];
        $result    = array();
        $paramsKey    = array();

        foreach ($spParams as $field => $value) {
            $paramsKey[] = ':' . $field;
        }

        $statement = $this->_dbAdapter->createStatement();
        $statement->prepare('CALL ' . $spDataSet . '( ' . implode(',', $paramsKey) . ' )');

        foreach ($spParams as $field => $value) {
            $statement->getResource()->bindParam( ':' . $field, $value ); 
        }

        return $this->fetchAllToArray( $statement );
    }*/

    public function postToDataSet( $config ) {
        $data            = $config['data'];
        $dataSet         = $config['dataSet'];
        $now             = date('Y-m-d H:i:s');

        $data['added']   = $now;
        $data['updated'] = $now;

        if( empty( $data['status'] ) ) {
            $data['status'] = 'I';
        }

        // remove id from data
        if( !empty( $data[ 'datarequestid'] ) ) {
            unset( $data[ 'datarequestid'] );
        }

        if( isset( $data[ 'id'] ) ) {
            unset( $data[ 'id'] );
        }

        return $this->insert( $dataSet, $data );
    }

    public function putToDataSet( $config ) {
        $data    = $config['data'];
        $dataSet = $config['dataSet'];
        $where   = $config['where'];
        $action  = $config['action'];
        $updated = date('Y-m-d H:i:s');

        if( !empty( $action ) && $action == 'delete' ) {
            $data['deleted'] = $updated;    
        } else {
            $data['updated'] = $updated;
        }
        

        if( empty( $data['status'] ) ) {
            $data['status'] = 'I';
        }

        // remove id from data
        if( !empty( $data[ 'datarequestid'] ) ) {
            unset( $data[ 'datarequestid'] );
        }

        if( isset( $data[ 'id'] ) ) {
            unset( $data[ 'id'] );
        }

        return $this->update( $dataSet, $data, $where );
    }

    // public function executeSp( $config ) {
    //     //delete id from $data
    //     $data       = $config['data'];
    //     $dataSource = $config['dataSource'];
    //     $params = array();

    //     if( !empty( $data[ 'id'] ) ) {
    //         unset( $data[ 'id'] );
    //     }

    //     $sql = 'CALL ';
    //     $sql.= $dataSource . ' ( ';

    //     foreach ($data as $key => $value) {
    //         $params[] = $value;
    //     }

    //     $params = implode( ',', $params );

    //     $sql .= $params . ' )';

    //     $statement = $this->_dbAdapter->createStatement( $sql );
    //     // print_r($statement->execute());
    //     // print_r($statement);
    //     return $this->fetchRowToArray( $statement );
    // }
}