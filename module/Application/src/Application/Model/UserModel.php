<?php

namespace Application\Model;

use Zend\Db\Sql\Expression;

class UserModel extends Table
{

    private $_name = 'hr_user';

    public function authenticate( $username, $password ) {
        $statement = $this->_dbAdapter->createStatement('CALL SP_Login("'. $username .'")');
        // print_r($statement->execute());
        // print_r($statement);
        return $this->fetchRowToArray( $statement );
    }

    public function getUserRole( $userId ) {
        $where = array(
            'hr_userRole.userId' => $userId,
            'hr_userRole.status' => 'A'
        );

        $roleFields = array(
            'id',
            'code',
            'name'
        );

        $select = $this->select()
                        ->from( 'hr_userRole' )
                        ->columns( array() )
                        ->join( 'hr_role', 'hr_role.id = hr_userRole.roleId', $roleFields )
                        ->where( $where );

        return $this->fetchRowToArray($select);
    }
}