<?php

namespace Application\Controller\Service;

use Zend\Db\Adapter\Adapter;
use Zend\Mvc\Controller\AbstractActionController;
use \Zend\Db\Sql\Where;

use \Exception;

class DBControl extends AbstractActionController
{

    private $_id;
    private $_filter;
    private $_start;
    private $_limit;
    private $_sort;
    private $_dataRequestModel;
    private $_data;
    private $_action;

    private $_drId;
    private $_drCode;
    private $_drName;
    private $_drType;
    private $_drSource;
    private $_drSet;
    private $_drRestriction;

    private $_resultCount;

    private $_modelControl;

    public function __construct() {
        
    }

    public function __set( $name, $value ) {
        $property = '_' . $name;

        $this->$property = $value;
    }

    public function __get( $name ) {
        $property = '_' . $name;

        return $this->$property;
    }

    private function _getSessionContainer() {
        return $this->_session()->getContainer();
    }

    private function _session() {
        $serviceLocator = $this->getServiceLocator();
        $session = $serviceLocator->get('Application\Controller\Service\Session');

        return $session;
    }

    public function executeDataRequest() {
        $serviceLocator      = $this->getServiceLocator();

        if( is_null( $this->_dataRequestModel ) ) {
            $this->_modelControl     = $serviceLocator->get('Application\Model\Model');
            $this->_dataRequestModel = $this->_modelControl->get('DataRequest');
        }

        try {
            $result = $this->_dataRequestModel->getDataRequest( $this->_id );

            if( !empty( $result ) ) {
                $this->_drId          = $result['id'];
                $this->_drCode        = $result['code'];
                $this->_drName        = $result['name'];
                $this->_drType        = $result['type'];
                $this->_drSource      = $result['source'];
                $this->_drSet         = $result['set'];
                $this->_drRestriction = !empty($result['restriction'])? $result['restriction'] : 'none';

                return $this->_checkRestriction();
            } else {
                throw new \Exception('No data request for id : ' . $this->_id );
            }
        } catch(\Exception $e)  {
            throw new \Exception($e->getMessage());
        }
    }

    private function _checkRestriction() {
        $sessionContainer = $this->_getSessionContainer();
        $userId           = $sessionContainer->userId;

        $userModel = $this->_modelControl->get( 'User' );
        $result = $userModel->getUserRole( $userId );

        switch ( strtolower( $this->_drRestriction ) ) {
            case 'superadmin':
                if( strtolower( $result['code'] ) == 'superadmin' ) {
                    return $this->_processRequest();
                } else {
                    throw new \Exception('Unauthorized request for data request id ' . $this->_drId);
                }
                break;
            case 'admin':
                throw new \Exception('Unauthorized request for data request id ' . $this->_drId);
            default:
                return $this->_processRequest();
                break;
        }
    }

    private function _processRequest() {
        switch( strtolower($this->_drType) ) {
            case 'get' :
                return $this->_getData();
            case 'post' :
                return $this->_postData();
            case 'put' :
                return $this->_putData();
        }
    }

    private function _getData() {
        switch( strtolower( $this->_drSource ) ) {
            case 'maindb':
                return $this->_getDataFromMainDb();
            case 'spmaindb':
                return $this->_executeSP();
        }
    }

    private function _postData() {
        switch( strtolower( $this->_drSource ) ) {
            case 'maindb':
                return $this->_postDataFromMainDb();
        }
    }

    private function _putData() {
        switch( strtolower( $this->_drSource ) ) {
            case 'maindb':
                return $this->_putDataFromMainDb();
            case 'spmaindb':
                return $this->_executeSP();
        }
    }

    private function _checkWhereRestriction( &$where ) {
        $sessionContainer = $this->_getSessionContainer();
        $userId           = $sessionContainer->userId;

        $userModel = $this->_modelControl->get( 'User' );
        $result = $userModel->getUserRole( $userId );

        switch ( strtolower( $this->_drRestriction ) ) {
            case 'user':
                if( empty( $where ) ) {
                    $where = new Where();
                    $where = $where->equalTo( 'userId', $userId );
                } else {
                    $where = $where->and
                                    ->equalTo( 'userId', $userId );
                }
                break;
            case 'userrole':
                // if( empty( $where ) ) {
                //     $where = new Where();
                //     $where = $where->equalTo( 'roleId', $result['id'] );
                // } else {
                //     $where = $where->and
                //                     ->equalTo( 'roleId', $result['id'] );
                // }
                break;
        }
    }

    private function _getDataFromMainDb() {
        $sessionContainer = $this->_getSessionContainer();
        $tenantId = $sessionContainer->tenantId;

        $where = $this->buildWhereClause( $this->_filter );

        if( $tenantId > 1 ) {
            if( empty( $where ) ) {
                $where = new Where();
                $where = $where->equalTo( 'tenantId', $tenantId );
            } else {
                $where = $where->and
                                ->equalTo( 'tenantId', $tenantId );
            }
        }

        $this->_checkWhereRestriction( $where );

        $config = array(
            'dataSet' => $this->_drSet,
            'where'   => $where,
            'offset'  => $this->_start,
            'limit'   => $this->_limit,
            'order'   => array()
        );

        $result = $this->_dataRequestModel->selectFromDataSet( $config );

        $config = array(
            'dataSet' => $this->_drSet,
            'where'   => $where
        );

        $this->_resultCount = count( $this->_dataRequestModel->selectFromDataSet( $config ) );

        return $result;
    }

    private function _postDataFromMainDb() {
        $sessionContainer = $this->_getSessionContainer();
        $userId           = $sessionContainer->userId;
        $tenantId         = $sessionContainer->tenantId;

        $this->_data['updatedBy'] = $userId;

        if( empty( $this->_data['tenantId'] ) ) {
            $this->_data['tenantId']  = $tenantId;
        }

        $config = array(
            'data'    => $this->_data,
            'dataSet' => $this->_drSet
        );

        $result = $this->_dataRequestModel->postToDataSet( $config );

        return $result;
    }

    private function _putDataFromMainDb() {
        $sessionContainer = $this->_getSessionContainer();
        $userId           = $sessionContainer->userId;
        $tenantId         = $sessionContainer->tenantId;

        $this->_data['updatedBy'] = $userId;
        
        if( empty( $this->_data['tenantId'] ) ) {
            $this->_data['tenantId']  = $tenantId;
        }

        if( !empty($this->_action) && $this->_action == 'delete' ) {
            $this->_data['status'] = 'D';   
        }

        $config = array(
            'data'    => $this->_data,
            'dataSet' => $this->_drSet,
            'action'  => $this->_action,
            'where'   => array(
                'id'  => $this->_data['id']
            )
        );

        $result = $this->_dataRequestModel->putToDataSet( $config );

        return $result;
    }

    private function _executeSP() {
        
        $spParams = $this->buildSPParams( $this->_filter );

        $config = array(
            'spDataSet' => $this->_drSet,
            'spParams'  => $spParams
        );

        $result             = $this->_dataRequestModel->executeSP( $config );
        $this->_resultCount = count( $result );

        return $result;
    }

    protected function buildWhereClause($filters, $where = null) {

        if(is_null($where)) {
            $where = new Where();
        }

        if( empty( $filters ) ) {
            return null;
        }

        /*(
            [op] => AND
            [set] => Array
                (
                    [0] => Array
                        (
                            [op] => AND
                            [field] => roleId
                            [value] => 2
                        )

                )

        )*/


        // echo '<pre>';
        // print_r($filters);

        if(is_array($filters)) {
            if(!empty($filters['op'])) {
                $op = $filters['op'];

                $filterSet = $filters['set'];

                if(count($filterSet) > 1) {
                    if($op == 'OR') {
                        $where = $where->nest();
                    }
                    
                }

                foreach ($filterSet as $key => $filter) {

                    if(!empty($filter['field'])) {

                        if($filter['bitOp'] == 'EQ') {
                            $where = $where->equalTo( $filter['field'], $filter['value']);
                        } else if($filter['bitOp'] == 'LIKE') {
                            $where = $where->like($filter['field'], '%' . $filter['value'] . '%');
                        }

                        if(count($filterSet) > 1 && $key < count($filterSet) - 1) {
                            if($filter['op'] == 'AND') {
                                $where = $where->and;    
                            } else if($filter['op'] == 'OR') {
                                $where = $where->or;
                            }
                            
                        }
                    } else if(!empty($filter['op'])) {
                        $where = $this->buildWhereClause($filter, $where);
                    }
                }

                if(count($filterSet) > 1) {
                    if($op == 'OR') {
                        $where = $where->unnest();
                    }
                    
                }
            }
        }

        return $where;
    }

    protected function buildSPParams( $filters ) {

        $sessionContainer = $this->_getSessionContainer();
        $tenantId         = $sessionContainer->tenantId;
        $userId           = $sessionContainer->userId;

        $spParams         = array();

        if(is_array($filters)) {
            if(!empty($filters['op'])) {
                $op = $filters['op'];

                $filterSet = $filters['set'];

                foreach ($filterSet as $key => $filter) {

                    if(!empty($filter['field'])) {
                        // echo $filter['value'] . ' ---- ' . gettype($filter['value']) . "\n";

                        if( gettype($filter['value']) == 'string' ) {
                            $filter['value'] = '"' . $filter['value'] . '"';
                        }
                        
                        $spParams[] = $filter['value'];
                    }
                }
            }
        }

        $spParams[] = $tenantId;
        $spParams[] = $userId;

        return implode( ',', $spParams);
    }
}