<?php

namespace Application\Controller\Service;

use Zend\Session\Container;

class Session {
    private static $_container;

    public function __construct() {
        if( empty( self::$_container ) ) {
            self::$_container = new Container( 'HrDr' );
        }
    }

    public function getContainer() {
        return self::$_container;
    }
}