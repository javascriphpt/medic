<?php
namespace Application\Controller;

use Application\Controller\Controller;


use Zend\View\Model\JsonModel;
use Zend\Crypt\Password\Bcrypt;

use Firebase\JWT\JWT;
use \Exception;

class AuthenticationController extends Controller
{

    public function indexAction()
    {
        $retVal = array(
            'success' => false,
            'errorMessage' => 'Unauthorized'
        );

        return new JsonModel($retVal);
    }

    public function getFormTokenAction() {
        $token  = $this->NoCSRF()->generate('token');

        $retVal = array(
            'success' => true,
            'token' => $token
        );

        return new JsonModel($retVal);
    }

    public function authenticateAction() {

        if( empty( $this->_sessionContainer ) ) {
            $this->setSessionContainer();
        }

        $retVal = array(
            'success' => true,
            'message' => 'Logged in'
        );

        $request = $this->getRequest();

        if ( $request->isPost() ) {
            $postData = $request->getPost();

            try {   
                $this->NoCSRF()->check( 'token', $postData, true, 60*10, false);


                $username = $postData['username'];
                $password = $postData['password'];
                
                $users = $this->model('User');
                $result = $users->authenticate($username, $password);

                //check if username existed
                if(!empty($result)) {
                    $securePassword = $result['password'];

                    $bcrypt = new Bcrypt();

                    //get password and verify if equal
                    if ($bcrypt->verify($password, $securePassword)) {

                        //check if account is active

                        if( strcasecmp(strtolower(trim($result['status'])), 'I') == 0) {
                            $retVal = array(
                                'success'      => false,
                                'errorMessage' => 'Account has been deactivated'
                            );
                        } else if( strcasecmp(strtolower(trim($result['status'])), 'D') == 0) {
                            $retVal = array(
                                'success'      => false,
                                'errorMessage' => 'Account does not exist.'
                            );
                        } else {

                            $tokenId    = base64_encode(mcrypt_create_iv(32));
                            $issuedAt   = time();
                            $notBefore  = $issuedAt + 5;             //Adding 10 seconds
                            $expire     = $notBefore + (60 * 60 * 24 );            // Adding 60 seconds
                            $serverName = 'DruidInc'; // Retrieve the server name from config file

                            /*
                             * Create the token as an array
                             */
                            $data = [
                                'iat'  => $issuedAt,         // Issued at: time when the token was generated
                                'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
                                'iss'  => $serverName,       // Issuer
                                'nbf'  => $notBefore,        // Not before
                                'exp'  => $expire,           // Expire
                                'data' => [                  // Data related to the signer user
                                    'userId'   => $result['userId'], // userid from the users table
                                    'userName' => $username, // User name
                                ]
                            ];

                            /**
                             * Extract the key
                             */
                            
                            $secretKey = base64_decode( $result['key'] );

                            $jwt = JWT::encode(
                                $data,      //Data to be encoded in the JWT
                                $secretKey, // The signing key
                                'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
                            );

                            $this->_sessionContainer->session = array(
                                $result['userId'] => $jwt 
                            );

                            $this->_sessionContainer->userId = $result['userId'];
                            $this->_sessionContainer->tenantId = $result['id'];
                            $this->_sessionContainer->key = $result['key'];

                            $retVal = array(
                                'success' => true,
                                'message' => 'Logged in',
                                'jwt'     => $jwt
                            );
                        }
                        
                    } 
                    else 
                    {
                        $retVal = array(
                            'success'      => false,
                            'errorMessage' => "Invalid password"
                        );
                    }
                }
                else
                {
                    $retVal = array(
                        'success'      => false,
                        'errorMessage' => "Invalid username/password"
                    );
                }
                
            } 
            catch(\Exception $e) 
            {
                $errorMessage = 'Invalid username/password';
                if(strpos($e->getMessage(), 'password') !== false)
                {
                    $errorMessage = 'Invalid username/password';
                }

                if(strpos(strtolower($e->getMessage()), 'csrf') !== false)
                {
                    $errorMessage = 'Invalid request source';
                }

                $retVal = array(
                    'success' => false,
                    'errorMessage' => $errorMessage . '. ' . $e->getMessage()
                );
            }

        }
        else
        {
            $retVal = array(
                'success'      => false,
                'errorMessage' => 'Logged out'
            );
        }

        return new JsonModel($retVal);
    }

    public function logoutAction()
    {
        if( empty( $this->_sessionContainer ) ) {
            $this->setSessionContainer();
        }

        $retVal = array(
            'success' => true,
            'message' => 'Logged out'
        );

        try
        {  
            $request = $this->getRequest();
            $authHeader = $request->getHeader('authorization');

            if( $authHeader ) {
                list($jwt) = sscanf( $authHeader->toString(), 'Authorization: Bearer %s');

                if ($jwt) {
                    try {
                        /*
                         * decode the jwt using the key from config
                         */
                        $secretKey = base64_decode($this->_sessionContainer->key);
                        
                        $token = JWT::decode($jwt, $secretKey, array('HS512'));

                        if( $this->_sessionContainer->session[ $this->_sessionContainer->userId ] == $jwt ) {
                            $this->_sessionContainer->getManager()->destroy();
                        } else {
                            $retVal = array(
                                'success' => false,
                                'errorMessage' => 'Invalid request source'
                            );
                        }

                    } catch (Exception $e) {
                        /*
                         * the token was not able to be decoded.
                         * this is likely because the signature was not able to be verified (tampered token)
                         */
                        
                        $retVal = array(
                            'success' => false,
                            'errorMessage' => 'Unauthorized'
                        );
                    }
                } else {
                    /*
                     * No token was able to be extracted from the authorization header
                     */
                    $retVal = array(
                        'success' => false,
                        'errorMessage' => 'Bad request. No token extracted from request.'
                    );
                }
            } else {
                $retVal = array(
                    'success' => false,
                    'errorMessage' => 'Token not found'
                );
            }

        } 
        catch(\Exception $e) 
        {
            $retVal = array(
                'success' => false,
                'errorMessage' => 'Invalid request source'
            );
        }

        
        return new JsonModel($retVal);
        
    }
}
