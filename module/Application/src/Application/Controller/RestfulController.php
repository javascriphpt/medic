<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Firebase\JWT\JWT;
use \Exception;

class RestfulController extends AbstractRestfulController {
    protected $_sessionContainer;
    protected $collectionOptions = array( 'GET', 'POST', 'PUT', 'DELETE' );
    protected $resourceOptions   = array( 'GET', 'PUT', 'DELETE' );

    public function setSessionContainer() {
        $this->_sessionContainer = $this->_session()->getContainer();
    }

    private function _session() {
        $serviceLocator = $this->getServiceLocator();
        $session = $serviceLocator->get('Application\Controller\Service\Session');

        return $session;
    }

    protected function _getOptions() {
        return $this->collectionOptions;
    }

    public function options() {
        $response = $this->getResponse();

        $response->getHeaders()
                ->addHeaderLine( 'Allow', implode( ',', $this->_getOptions() ) );

        return $response;
    }

    public function setEventManager( \Zend\EventManager\EventManagerInterface $events ) {
        parent::setEventManager($events);

        $this->events = $events;

        $events->attach( 'dispatch', array( $this, 'checkOptions' ), 10 );
    }

    public function checkOptions( $e ) {
        if( in_array( $e->getRequest()->getMethod(), $this->_getOptions() ) ) {
            return;
        }

        // Method not allowed
        $response = $this->getResponse();
        $response->setStatusCode( 405 );

        return $response;
    }

    protected function model($modelName) {
        $serviceLocator = $this->getServiceLocator();
        $model = $serviceLocator->get('Application\Model\Model');

        return $model->get($modelName);
    }

    protected function dbControl() {
        $serviceLocator = $this->getServiceLocator();
        $dbControl = $serviceLocator->get('Application\Controller\Service\DBControl');

        return $dbControl;
    }

    protected function isProduction() {
        if(!empty($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] == 'production') {
            return true;
        }

        return false;
    }

    private function _checkToken( \Zend\Http\Request $request ) {
        if( empty( $this->_sessionContainer ) ) {
            $this->setSessionContainer();
        }

        $authHeader = $request->getHeader('authorization');

        if( $authHeader ) {
            list($jwt) = sscanf( $authHeader->toString(), 'Authorization: Bearer %s');

            if ($jwt) {
                try {
                    /*
                     * decode the jwt using the key from config
                     */
                    $secretKey = base64_decode($this->_sessionContainer->key);
                    
                    $token = JWT::decode($jwt, $secretKey, array('HS512'));
                } catch (Exception $e) {
                    /*
                     * the token was not able to be decoded.
                     * this is likely because the signature was not able to be verified (tampered token)
                     */
                    header('HTTP/1.0 401 Unauthorized');
                    throw new Exception('Unauthorized. ' . $e->getMessage());
                }
            } else {
                /*
                 * No token was able to be extracted from the authorization header
                 */
                header('HTTP/1.0 400 Bad Request');
                throw new Exception('Bad Request. No authorization header');
            }
        } else {
            header('HTTP/1.0 400 Bad Request');
            throw new Exception('Bad Request.');
        }
    }

    public function get( $id ) {
        $request = $this->getRequest();

        try {
            $this->_checkToken( $request );
        } catch( \Exception $e ) {
            throw new Exception( $e->getMessage() );
        }
    }

    public function create( $data ) {
        $request = $this->getRequest();

        try {
            $this->_checkToken( $request );
        } catch( \Exception $e ) {
            throw new Exception( $e->getMessage() );
        }
    }

    public function update( $id, $data ) {
        $request = $this->getRequest();

        try {
            $this->_checkToken( $request );
        } catch( \Exception $e ) {
            throw new Exception( $e->getMessage() );
        }
    }

    public function delete( $id ) {
        $request = $this->getRequest();

        try {
            $this->_checkToken( $request );
        } catch( \Exception $e ) {
            throw new Exception( $e->getMessage() );
        }
    }
}