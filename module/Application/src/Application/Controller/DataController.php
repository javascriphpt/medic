<?php
namespace Application\Controller;

use Application\Controller\Controller;
use Zend\View\Model\JsonModel;
use Zend\Json\Json;

use \Exception;

class DataController extends RestfulController
{

    public function index()
    {
        $retVal = array(
            'success' => false,
            'errorMessage' => 'Unauthorized'
        );

        return new JsonModel($retVal);
    }

    public function get( /*Data Request Id */ $id ) {
        $retVal = array();

        try {
            $filter   = !empty($this->getRequest()->getQuery('filter')) ? Json::decode( stripslashes( $this->getRequest()->getQuery('filter') ), Json::TYPE_ARRAY ) : array();
            $start    = $this->getRequest()->getQuery('start');
            $limit    = $this->getRequest()->getQuery('limit');
            $sort     = !empty($this->getRequest()->getQuery('sort')) ? Json::decode( stripslashes( $this->getRequest()->getQuery('sort') ), Json::TYPE_ARRAY ) : array();
            
            parent::get( $id );

            $dbControl         = $this->dbControl();
            $dbControl->id     = $id;
            $dbControl->filter = $filter;
            $dbControl->start  = $start;
            $dbControl->limit  = $limit;
            $dbControl->sort   = $sort;

            $result = $dbControl->executeDataRequest();
            $retVal = $this->_composeReturnData(
                true,
                '',
                $dbControl->resultCount,
                $result
            );

        } catch ( \Exception $e ) {
            $retVal = $this->_composeReturnData(
                false,
                'Error on request. <br.>' . $e->getMessage() . '<br/> Please contact administrator',
                0,
                array()
            );
        }

        return new JsonModel( $retVal );
    }

    public function create( $data ) {
        $retVal = array();

        try {

            parent::create( $data );

            $dbControl         = $this->dbControl();
            $dbControl->id     = $data[ 'datarequestid' ];
            $dbControl->data   = $data;
 
            $result = $dbControl->executeDataRequest();
            $retVal = $this->_composeReturnData(
                true,
                'Record added successfully',
                $result,
                array()
            );

        } catch ( \Exception $e ) {
            $retVal = $this->_composeReturnData(
                false,
                'Error on request. <br.>' . $e->getMessage() . '<br/> Please contact administrator',
                0,
                array()
            );
        }

        return new JsonModel( $retVal );
    }

    public function update( $id, $data ) {
        $retVal = array();

        try {

            parent::update( $id, $data );

            $dbControl         = $this->dbControl();
            $dbControl->id     = $data[ 'datarequestid' ];
            $dbControl->data   = $data;
 
            $result = $dbControl->executeDataRequest();
            $retVal = $this->_composeReturnData(
                true,
                'Record updated successfully',
                $result,
                array()
            );

        } catch ( \Exception $e ) {
            $retVal = $this->_composeReturnData(
                false,
                'Error on request. <br.>' . $e->getMessage() . '<br/> Please contact administrator',
                0,
                array()
            );
        }

        return new JsonModel( $retVal );
    }

    public function delete( $id ) {
        $retVal = array();

        try {

            parent::delete( $id );

            $dataId     = !empty($this->getEvent()->getRouteMatch()->getParam('recordid')) ? $this->getEvent()->getRouteMatch()->getParam('recordid') : 0;

            $dbControl         = $this->dbControl();
            $dbControl->id     = $id;
            $dbControl->action = 'delete';
            $dbControl->data   = array(
                'id' => $dataId
            );
 
            $result = $dbControl->executeDataRequest();
            $retVal = $this->_composeReturnData(
                true,
                'Record deleted successfully',
                $result,
                array()
            );

        } catch ( \Exception $e ) {
            $retVal = $this->_composeReturnData(
                false,
                'Error on request. <br.>' . $e->getMessage() . '<br/> Please contact administrator',
                0,
                array()
            );
        }

        return new JsonModel( $retVal );
    }

    private function _composeReturnData( $success, $message, $total, $data ){
        return array(
            'success' => $success,
            'message' => $message,
            'total'   => $total,
            'data'    => $data,
            '_links' => array(
                'self' => array(
                    'href' => $this->getRequest()->getUriString()
                )
            )
        );
    }
}
